#!/bin/bash

if [[ -n "$1" ]]; then
	ps -ef | grep "$1" | awk '{print substr($2,0)}' | xargs sudo kill
else
	echo "Undefined target!"
fi

