#!/bin/bash

if [[ -n "$2" ]]; then
	echo "Create whith standart symlink ( $1 <--> $2 )"
	socat -d -d pty,link=$1,raw,echo=0 pty,link=$2,raw,echo=0
else
	echo "Create whith standart symlink ( /dev/socatport1 <--> /dev/socatport2 )"
	socat -d -d pty,link="/dev/socatport1",raw,echo=0 pty,link="/dev/socatport2",raw,echo=0
fi
